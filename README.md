# Es Divendres?

Aquesta repo conté codi que fa correr el bot de telegram es_divendres_bot.

## Execució en local

Per tal de poder executar en local el bot, es necessari l'ús d'un fixter `.env` dins el mateix directori.
Aquest fitxer ha de contenir el token que et proveeix el Father Bot:

```
TELEGRAM_API_TOKEN="<el token va aqui>"
```

Després, instal·lar dependències fent

```console
poetry install
```

Finalment, executar el bot amb:

```console
poetry run python ./bot.py
```
