import logging
import os
import random

import dotenv
from telegram import Update
from telegram.ext import CallbackContext, CommandHandler, Updater

dotenv.load_dotenv()

logging.basicConfig(
    level=logging.INFO,
    style="{",
    format="{name} -- [{levelname}] {asctime}: {message}",
)

random.seed()

TELEGRAM_API_TOKEN = os.getenv("TELEGRAM_API_TOKEN")
WEBHOOK_URL = os.getenv("WEBHOOK_URL")
PORT = int(os.getenv("PORT", "8443"))


def question(update: Update, context: CallbackContext) -> None:

    update.message.reply_text(
        random.choices(
            ["Yes!", "No", "I don't know, why you ask me?"],
            weights=[10, 89, 1],
        ).pop(),
    )


def start_bot():
    updater = Updater(TELEGRAM_API_TOKEN)
    updater.dispatcher.add_handler(CommandHandler("kubernetes", question))

    if os.getenv("ENV"):
        updater.start_webhook(
            listen="0.0.0.0",
            port=PORT,
            url_path=TELEGRAM_API_TOKEN,
            webhook_url=f"{WEBHOOK_URL}/{TELEGRAM_API_TOKEN}",
        )
    else:
        logging.info("Starting in local polling mdoe")
        updater.start_polling()

    updater.idle()


if __name__ == "__main__":
    start_bot()
